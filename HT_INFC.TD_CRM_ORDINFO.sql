CREATE OR REPLACE PROCEDURE HT_INFC.td_CRM_ORDINFO(
    I_PROCESS_START_DATE IN VARCHAR,
    I_PROCESS_END_DATE   IN VARCHAR)
AS
  /*-----------------------------------------------------------------------------
  Description: This procedure extracts order data from OMSPRD schema and inserts
  into staging tables in td_INFC
  05/27/2020 Mani - Bugfix for demand descripency
  08/11/2020 Mani - parallel hint to the Ship CTE 
  ------------------------------------------------------------------------------*/
  v_step                 VARCHAR2(500);
  v_I_PROCESS_START_DATE DATE;
  v_I_PROCESS_END_DATE   DATE;
BEGIN
  v_I_PROCESS_START_DATE := to_date(I_PROCESS_START_DATE||' 00:00:00','dd-MON-yyyy HH24:MI:SS');
  --to_date(I_PROCESS_START_DATE,'dd-MON-yyyy HH24:MI:SS')
  v_I_PROCESS_END_DATE := to_date(I_PROCESS_END_DATE||' 23:59:59','dd-MON-yyyy HH24:MI:SS');
  --v_I_PROCESS_START_DATE :=  to_date(I_PROCESS_START_DATE,'dd-MON-YYYY') ;
  --v_I_PROCESS_END_DATE :=  to_date(I_PROCESS_END_DATE,'dd-MON-YYYY') ;

  v_step:= 'Delete old data from td_CRM_ORDHEAD';
  --DELETE FROM td_CRM_ORDHEAD;
  execute immediate 'truncate table td_CRM_ORDHEAD';

  v_step:= 'Insert basic data into td_CRM_ORDHEAD';
  INSERT
  INTO td_CRM_ORDHEAD
    (
      PURCHASE_ORDERS_ID,
      PARENT_PURCHASE_ORDERS_ID,
      OrderNumber,
      CustomerNumber,
      LoyaltyNumber,
      OrderDate,
      MerchandiseAmt,
      TOTALORDERAMT,
      EnteredLocation,
      TitleCode,
      WebOrderNumber,
      DATETIME,
      CREATED_DATETIME,
      Order_category,
      CSR_ID,
      CURRENCY_CODE
    )
    (SELECT p.PURCHASE_ORDERS_ID PURCHASE_ORDERS_ID,
        p.PARENT_PURCHASE_ORDERS_ID,
        P.TC_Purchase_Orders_Id     AS OrderNumber,
        P.Customer_Code             AS CustomerNumber,
        pf.ref_field2               AS LoyaltyNumber,
        P.Purchase_Orders_Date_DTTM AS OrderDate,
        p.Monetary_Value            AS MerchandiseAmt,
        p.GRAND_TOTAL               AS TOTALORDERAMT,
        P.Entry_Code                AS EnteredLocation,
        PF.Ref_FIELD1               AS TitleCode,
        P.TC_Purchase_Orders_Id     AS WebOrderNumber,
        p.PURCHASE_ORDERS_DATE_DTTM AS DATETIME,
        p.CREATED_DTTM CREATED_DATETIME,
        p.Order_category,
        CASE
          WHEN P.Order_category IN (3)
          THEN P.Created_Source
        END   AS CSR_ID,
        'USD' AS CURRENCY_CODE
      FROM ca.PURCHASE_ORDERS p,
        ca.PO_REF_FIELDS PF
      WHERE (p.last_updated_dttm > v_I_PROCESS_START_DATE AND P.Purchase_Orders_Date_DTTM <= v_I_PROCESS_END_DATE) --BETWEEN v_I_PROCESS_START_DATE AND v_I_PROCESS_END_DATE
      AND p.PURCHASE_ORDERS_ID      = PF.PURCHASE_ORDERS_ID
      AND ( p.ORDER_CATEGORY NOT   IN (3, 83)
      OR (p.ORDER_CATEGORY         IN (3)
      AND p.PURCHASE_ORDERS_STATUS != 20))
    );

  v_step:= 'Update TaxAMT in td_CRM_ORDHEAD';
  UPDATE td_CRM_ORDHEAD p
  SET TAXAMT=
    (SELECT SUM (TAX_AMOUNT)
    FROM ca.A_TAX_DETAIL t
    WHERE p.PURCHASE_ORDERS_ID = t.ENTITY_ID
    AND T.MARK_FOR_DELETION    = 0
    GROUP BY Entity_ID
    );

  V_STEP:= 'Update shippingAmt in td_CRM_ORDHEAD';
  UPDATE td_CRM_ORDHEAD p
  SET shippingamt=
    (SELECT SUM(charge_amount)
    FROM ca.A_CHARGE_DETAIL d
    WHERE P.Purchase_Orders_ID = D.Entity_ID
  --  AND d.Entity_Line_ID      IS NULL
    AND d.CHARGE_CATEGORY      = 20
    AND d.MARK_FOR_DELETION    = 0
    GROUP BY entity_id
    );

  v_step:= 'Update OrderMethod in td_CRM_ORDHEAD';
  UPDATE td_CRM_ORDHEAD p
  SET OrderMethod =
    (SELECT order_type
    FROM ca.ORDER_TYPE OT
    WHERE P.Order_category = OT.Order_Type_ID
    );

  v_step:= 'Update ShippingDiscount in td_CRM_ORDHEAD';
  UPDATE td_crm_ordhead p
  SET ShippingDiscountAmt =
    (SELECT SUM(Discount_Amount)
    FROM CA.A_DISCOUNT_DETAIL DD
    WHERE P.Purchase_Orders_ID = DD.Entity_ID
    AND DD.ENTITY_LINE_ID     IS NULL
    AND upper (DD.EXTERNAL_DISCOUNT_ID) LIKE '%SHIPPING%'
    AND dd.MARK_FOR_DELETION = 0
    GROUP BY dd.entity_id
    );

  v_step:= 'Update original order details for return orders in td_CRM_ORDHEAD';
  UPDATE td_CRM_ORDHEAD p
  SET
    (
      ORIGINALORDER,
      ORIGINALTITLECODE,
      ORIGDATETIME,
      ORIGORDERMETHOD
    )
    =
    (SELECT POR.TC_PURCHASE_ORDERS_ID AS OriginalOrder,
      PFR.REF_FIELD1                  AS ORIGINALTITLECODE,
      POR.PURCHASE_ORDERS_DATE_DTTM   AS ORIGDATETIME,
      OC.ORDER_TYPE
    FROM ca.Purchase_Orders POR,
      ca.PO_REF_FIELDS PFR,
      ca.ORDER_TYPE OC
    WHERE p.PARENT_PURCHASE_ORDERS_ID = POR.PURCHASE_ORDERS_ID
    AND POR.PURCHASE_ORDERS_ID        = PFR.PURCHASE_ORDERS_ID
    AND POR.ORDER_CATEGORY            = OC.ORDER_TYPE_ID
    );

  v_step:= 'Update titlecode if no titlecode exists for parent or actual order in TD_CRM_ORDHEAD';
  UPDATE TD_CRM_ORDHEAD
  SET TITLECODE=
    (SELECT DECODE(MIN(BRAND), 'HT','HotTopic','TD','Torrid','HU','HerUniverse','BL','BoxLunch')
    FROM CA.ITEM_CBO
    WHERE ITEM_NAME IN
      (SELECT SKU
      FROM CA.PURCHASE_ORDERS_LINE_ITEM
      WHERE PURCHASE_ORDERS_LINE_ITEM.PURCHASE_ORDERS_ID= TD_CRM_ORDHEAD.PURCHASE_ORDERS_ID
      )
    )
  WHERE TITLECODE       IS NULL
  AND ORIGINALTITLECODE IS NULL;

  update td_CRM_ORDHEAD coh set taxamt=
(select nvl(abs(sum(td.tax_amount)),0) from
  ca.a_invoice i,
  ca.A_INVOICE_LINE il,
  CA.PURCHASE_ORDERS po,
  ca.A_TAX_DETAIL td--,
  -- TD_CRM_ORDHEAD coh
  where po.tc_purchase_orders_id =coh.ORIGINALORDER
  and po.purchase_orders_id=i.entity_id
  and i.invoice_id=il.invoice_id
  and i.invoice_id = td.entity_id
  and il.invoice_line_id=td.entity_line_id
  and (coh.taxamt is null or coh.taxamt=0)
  and i.return_order_id=coh.purchase_orders_id),
  coh.MERCHANDISEAMT=
(select coh.MERCHANDISEAMT-nvl(abs(sum(td.tax_amount)),0) from
  ca.a_invoice i,
  ca.A_INVOICE_LINE il,
  CA.PURCHASE_ORDERS po,
  ca.A_TAX_DETAIL td--,
  -- TD_CRM_ORDHEAD coh
  where po.tc_purchase_orders_id =coh.ORIGINALORDER
  and po.purchase_orders_id=i.entity_id
  and i.invoice_id=il.invoice_id
  and i.invoice_id = td.entity_id
  and il.invoice_line_id=td.entity_line_id
  and (coh.taxamt is null or coh.taxamt=0)
  and i.return_order_id=coh.purchase_orders_id)
  where ordermethod='Return'
  and (coh.taxamt is null or coh.taxamt=0);


  v_step                := 'Delete old data from td_CRM_ORDDTL';
  --DELETE FROM td_CRM_ORDDTL;
  execute immediate 'truncate table td_CRM_ORDDTL';

  v_step:= 'Insert into td_CRM_ORDDTL';
  INSERT
  INTO td_CRM_ORDDTL
    (
      PURCHASE_ORDERS_ID,
      ORDERNUMBER,
      ORDERDATE,
      CUSTOMERNUMBER,
      LOYALTYNUMBER,
      TITLECODE,
      CLIENTLNENUM,
      MERCHTYPE,
      ITEMNUMBER,
      DOLLARVALUENET,
      DOLLARVALUEGROSS,
      LINETAXAMOUNT,
      LINEDISCOUNT,
      QUANTITY,
      ORDERMETHOD,
      SHIPTYPE,
      PURCHASE_ORDERS_LINE_STATUS,
      ENTEREDLOCATION,
      ORIGORDERNUMBER,
      ORIGORDERDATE,
      ORIGORDERMETHOD,
      ORIGSHIPTYPE,
      IS_GIFT,
      ISGC,
      ISDONATION,
      ISPRESALE,
      ISBACKORDER,
      CANCELREASONCODE,
      CANCELREASONDESC,
      CANCELDATE,
      RETURNREASONCODE,
      RETURNREASONDESC,
      CSRID,
      USEDEMAIL,
      USEDPHONENBR,
      BILL_TO_FIRST_NAME,
      BILL_TO_LAST_NAME,
      BILL_TO_ADDRESS_LINE1,
      BILL_TO_ADDRESS_LINE2,
      BILL_TO_CITY,
      BILLTO_STATE_PROV,
      BILL_TO_POSTAL_CODE,
      BILL_TO_COUNTRY_CODE,
      D_NAME,
      D_LAST_NAME,
      D_ADDRESS_1,
      D_ADDRESS_2,
      D_CITY,
      D_STATE_PROV,
      D_POSTAL_CODE,
      D_COUNTRY_CODE,
      O_FACILITY_ALIAS_ID,
      SHIPPED_QTY,
      CANCELQTY,
      BACKORDERQTY,
      BACKORDERACTUALFULFILLMENTDATE,
      PICKUPSTORE,
      ISABANDONED,
      PICKEDUP,
      SHIP_LAST_UPD_DT,
      SHIP_METHOD,ISBOPIS,ISSFS
    )
  SELECT *
  FROM
    ( WITH PO_CTE AS
    ( SELECT PURCHASE_ORDERS_ID, created_datetime FROM td_CRM_ORDHEAD
    ),
    SHP_CTE AS
    (SELECT
      /*+ LEADING(P) PARALLEL*/
      p.PURCHASE_ORDERS_ID,
      PL.SKU,
      NVL( PL.Root_Line_Item_ID, PL.PURCHASE_ORDERS_LINE_ITEM_ID ) AS ClientLneNum,
      O.O_facility_alias_ID,
      SUM(NVL(OL.SHIPPED_QTY, 0) ) AS SHIPPED_QTY,
      MAX(OL.LAST_UPDATED_DTTM)    AS LAST_UPDATED_DTTM,
      MAX(sc.ship_method)  as ship_method,
      CASE WHEN PL.ORDER_FULFILLMENT_OPTION IN('03') AND O.O_FACILITY_ALIAS_ID not in (select to_char(O_FACILITY_ALIAS_ID) from HT_INFC.HTTD_FACILITY_ID_CONFIG where brand='TD') AND SUBSTR(O.O_FACILITY_ALIAS_ID,1,2)<>'VF' THEN 1 ELSE 0 END AS ISSFS
    FROM td_CRM_ORDHEAD p
    JOIN ca.PURCHASE_ORDERS_LINE_ITEM PL
    ON p.PURCHASE_ORDERS_ID = PL.PURCHASE_ORDERS_ID --and PL.CREATED_DTTM >= p.CREATED_DATETIME
    JOIN ca.ORDERS O
    ON p.PURCHASE_ORDERS_ID = O.PURCHASE_ORDER_ID
	  AND (o.O_FACILITY_ALIAS_ID IN (select to_char(O_FACILITY_ALIAS_ID) from HT_INFC.HTTD_FACILITY_ID_CONFIG where brand='TD') OR o.O_FACILITY_ALIAS_ID IN(DECODE(PL.ORDER_FULFILLMENT_OPTION,'01',PL.O_FACILITY_ALIAS_ID)) OR (PL.ORDER_FULFILLMENT_OPTION IN('03') AND O.O_FACILITY_ALIAS_ID not in (select to_char(O_FACILITY_ALIAS_ID) from HT_INFC.HTTD_FACILITY_ID_CONFIG where brand='TD') AND SUBSTR(O.O_FACILITY_ALIAS_ID,1,2)<>'VF'))
    AND O.CREATED_DTTM     >= CREATED_DATETIME
    JOIN ca.Order_line_Item OL
    ON O.Order_id                       = OL.Order_id
    AND PL.PURCHASE_ORDERS_LINE_ITEM_ID = OL.MO_LINE_ITEM_ID
    AND ol.created_dttm                >= created_datetime
    AND OL.SHIPPED_QTY                 IS NOT NULL
    LEFT JOIN ca.lpn l ON O.tc_order_id=l.tc_order_id
    --LEFT JOIN ca.lpn_detail lp ON l.lpn_id = lp.lpn_id AND  ol.tc_order_line_id = lp.tc_order_line_id
    LEFT JOIN (SELECT b. ship_via,b.description ship_method,a.code_desc
            FROM ca.SYS_CODE a, dom_mda.ship_via b
           WHERE a.code_type='018' and a.rec_type='C'  and a.code_id = b.ship_via) sc
      ON  l.ship_via=sc.ship_via
    GROUP BY p.PURCHASE_ORDERS_ID,
      PL.SKU,
      NVL( PL.Root_Line_Item_ID, PL.PURCHASE_ORDERS_LINE_ITEM_ID ),
      O.O_facility_alias_ID,
      CASE WHEN PL.ORDER_FULFILLMENT_OPTION IN('03') AND O.O_FACILITY_ALIAS_ID not in (select to_char(O_FACILITY_ALIAS_ID) from HT_INFC.HTTD_FACILITY_ID_CONFIG where brand='TD') AND SUBSTR(O.O_FACILITY_ALIAS_ID,1,2)<>'VF' THEN 1 ELSE 0 END
    ),
    CANCEL_CTE AS
    (SELECT
      /*+ LEADING(P)*/
      p.PURCHASE_ORDERS_ID,
      PL.SKU,
      NVL( PL.Root_Line_Item_ID, PL.PURCHASE_ORDERS_LINE_ITEM_ID ) AS ClientLneNum,--O.O_facility_alias_ID ,
      SUM(PL.CANCELLED_QTY)                                        AS CANCELQTY
    FROM td_CRM_ORDHEAD P--C join ca.Purchase_orders P on PC.Purchase_Orders_ID = PC.Purchase_Orders_ID
    JOIN ca.PURCHASE_ORDERS_LINE_ITEM PL
    ON P.Purchase_orders_ID         = PL.Purchase_Orders_ID
    AND Pl.Parent_PO_Line_Item_ID  IS NULL
    WHERE NVL(PL.Cancelled_QTY, 0) <> 0
    GROUP BY P.Purchase_Orders_ID,
      PL.SKU,
      NVL( PL.Root_Line_Item_ID, PL.PURCHASE_ORDERS_LINE_ITEM_ID )
    ),
    STS_CTE AS
    (SELECT
      /*+ LEADING(PC)*/
      PC.Purchase_Orders_ID,
      O.Order_ID,
      OL.ITEM_NAME,
      O.O_FACILITY_ALIAS_ID,
      OL.DO_DTL_STATUS,
      MO_LINE_ITEM_ID,
      CASE
        WHEN OL.DO_DTL_STATUS = 190
        THEN OL.LAST_UPDATED_DTTM
      END AS SHIPPEDDATE,
      CASE
        WHEN OL.DO_DTL_STATUS = 200
        AND RC.REASON_CODE    = 'AO'
        THEN 1
        ELSE 0
      END AS ISABANDONED,
      CASE
        WHEN OL.DO_DTL_STATUS = 190
        THEN O.O_FACILITY_ALIAS_ID
      END AS PickUpStore
    FROM td_CRM_ORDHEAD PC
    JOIN ca.ORDERS O
    ON PC.PURCHASE_ORDERS_ID = O.PURCHASE_ORDER_ID
    AND O.CREATED_DTTM      >= CREATED_DATETIME
    JOIN ca.ORDER_LINE_ITEM OL
    ON O.Order_ID        = OL.Order_ID
    AND ol.created_dttm >= created_datetime
    LEFT OUTER JOIN ca.ORDER_REASON_CODE RC
    ON OL.order_id                 = rc.order_id
    AND Ol.line_item_id            = rc.Order_line_ID
    WHERE O.Delivery_Options       in ('01','02')
    AND O.O_FACILITY_ALIAS_ID NOT IN ( '899', '897', 'DDC' )
    ),
    Bill_CTE AS
    (SELECT PC.PURCHASE_ORDERS_ID,
      MIN(PD.BILL_TO_FIRST_NAME) BILL_TO_FIRST_NAME,
      MIN(PD.BILL_TO_LAST_NAME) BILL_TO_LAST_NAME,
      MIN(PD.BILL_TO_ADDRESS_LINE1) BILL_TO_ADDRESS_LINE1,
      MIN(PD.BILL_TO_ADDRESS_LINE2) BILL_TO_ADDRESS_LINE2,
      MIN(PD.BILL_TO_CITY) BILL_TO_CITY,
      MIN(PD.BILLTO_STATE_PROV) BILLTO_STATE_PROV,
      MIN(PD.BILL_TO_POSTAL_CODE) BILL_TO_POSTAL_CODE,
      MIN(PD.BILL_TO_COUNTRY_CODE) BILL_TO_COUNTRY_CODE
    FROM td_CRM_ORDHEAD PC
    JOIN CA.A_PAYMENT_DETAIL PD
    ON PC.PURCHASE_ORDERS_ID = PD.ENTITY_ID
    AND CHARGE_SEQUENCE      = 1
    AND IS_SUSPENDED         = 0
    GROUP BY PC.PURCHASE_ORDERS_ID
    ),
    TAX_CTE AS
    (SELECT eNTITY_id,
      entity_line_id,
      SUM(NVL(Tax_Amount,0)) AS Tax_Amount
    FROM ca.A_TAX_DETAIL l
    JOIN td_CRM_ORDHEAD C
    ON L.Entity_ID            = C.Purchase_orders_ID
    WHERE l.mark_for_deletion = 0
    GROUP BY ENTITY_ID,
      ENTITY_LINE_ID
    )
  SELECT a.Purchase_Orders_ID,
    a.OrderNumber,
    a.OrderDate,
    a.CustomerNumber,
    a.LoyaltyNumber,
    a.TitleCode,
    a.ClientLneNum,
    a.MerchType,
    a.ItemNumber,
    a.DollarValueNet,
    a.DollarValueGross,
    a.LINETAXAMOUNT,
    a.LINEDISCOUNT,
    a.Quantity,
    a.ORDERMETHOD,
    a.ShipType,
    a.Purchase_Orders_Line_Status,
    a.EnteredLocation,
    a.OrigOrderNumber,
    a.OrigOrderDate,
    a.ORIGORDERMETHOD,
    a.OrigShipType,
    a.IS_GIFT,
    a.ISGC,
    a.IsDonation,
    a.IsPreSale,
    a.ISBACKORDER,
    a.CancelReasonCode,
    a.CANCELREASONDESC,
    a.CANCELDATE,
    a.RETURNREASONCODE,
    a.RETURNREASONDESC,
    a.CSRID,
    a.UsedEmail,
    a.UsedPhoneNbr,
    a.BILL_TO_FIRST_NAME,
    a.BILL_TO_LAST_NAME,
    a.BILL_TO_ADDRESS_LINE1,
    a.BILL_TO_ADDRESS_LINE2,
    a.BILL_TO_CITY,
    a.BILLTO_STATE_PROV,
    a.BILL_TO_POSTAL_CODE,
    a.BILL_TO_COUNTRY_CODE,
    a.D_Name,
    a.D_Last_Name,
    a.D_Address_1,
    a.D_Address_2,
    a.D_CITY,
    a.D_State_Prov,
    a.D_POSTAL_CODE,
    a.D_Country_Code,
    SC.O_FACILITY_ALIAS_ID AS O_FACILITY_ALIAS_ID,
    SC.SHIPPED_QTY,
    CC.CANCELQTY,
    CASE
      WHEN ISBACKORDER = 1
      THEN a.QUANTITY - SC.SHIPPED_QTY
      ELSE 0
    END BACKORDERQTY,
    CASE
      WHEN IsBackOrder    = 1
      AND SC.Shipped_qty IS NOT NULL
      THEN SC.Last_Updated_DTTM
      ELSE NULL
    END AS BackOrderActualFulfillmentDate,
    STS.PICKUPSTORE,
    STS.IsAbandoned,
    ShippedDate          AS PickedUp,
    SC.Last_Updated_DTTM AS SHIP_LAST_UPD_DT,
    SC.ship_method,a.ISBOPIS,SC.ISSFS
  FROM
    (SELECT
      /*+ LEADING(PC) */
      P.Purchase_Orders_ID,
      P.TC_Purchase_Orders_Id              AS OrderNumber,
      P.Purchase_Orders_Date_DTTM          AS OrderDate,
      P.Customer_Code                      AS CustomerNumber,
      pf.ref_field2                        AS LoyaltyNumber,
      NVL( PF.REF_FIELD1, PFR.REF_FIELD1 ) AS TITLECODE,
      PL.PURCHASE_ORDERS_LINE_ITEM_ID POLIID,
      PL.PURCHASE_ORDERS_LINE_ITEM_ID CLIENTLNENUM, --PL.TC_PO_LINE_ID CLIENTLNENUM,
      CASE
        WHEN NVL(PLF.Ref_nUM3, 0) = 0
        THEN 'Sellable'
        ELSE 'Non-Merch'
      END    AS MerchType,
      PL.SKU AS ItemNumber,
      ( PL.Unit_Monetary_Value *
      CASE
        WHEN P.Order_category = 23
        THEN PL.ORDER_QTY
        ELSE PL.ORIG_ORDER_QTY
      END )                    - SUM(NVL(DD.DISCOUNT_AMOUNT, 0) ) AS DollarValueNet,
      ( PL.Unit_Monetary_Value *
      CASE
        WHEN P.Order_category = 23
        THEN PL.ORDER_QTY
        ELSE PL.ORIG_ORDER_QTY
      END )                            AS DollarValueGross,
      SUM(NVL(Tax_Amount, 0) )         AS LineTaxAmount,
      SUM(NVL(DD.DISCOUNT_AMOUNT, 0) ) AS LINEDISCOUNT,
      CASE
        WHEN P.Order_category = 23
        THEN PL.ORDER_QTY
        ELSE PL.ORIG_ORDER_QTY
      END                                                                                   AS Quantity,
      OT.ORDER_TYPE                                                                         AS ORDERMETHOD,
      DECODE( pl.ORDER_FULFILLMENT_OPTION, '03', 'Ship to address', '02', 'Ship to store','01','BOPIS') AS ShipType,
      pl.Purchase_Orders_Line_Status,
      P.Entry_Code                                                                           AS EnteredLocation,
      PR.TC_Purchase_Orders_ID                                                               AS OrigOrderNumber,
      PR.Purchase_Orders_Date_DTTM                                                           AS OrigOrderDate,
      ODT.ORDER_TYPE                                                                         AS ORIGORDERMETHOD,
      DECODE( plr.ORDER_FULFILLMENT_oPTION, '03', 'Ship to address', '02', 'Ship to store','01','BOPIS') AS OrigShipType,
      PL.Is_Gift,
      CASE
        WHEN c.REF_FIELD1 = 'Y'
        THEN 1
        ELSE 0
      END AS ISGC,
      CASE
        WHEN C.Ref_Field2 = 'Y'
        THEN 1
        ELSE 0
      END AS IsDonation,
      CASE
        WHEN plf.Ref_Num5 IN ( 1, 2 )
        THEN 1
        ELSE 0
      END AS IsPreSale,
      CASE
        WHEN PLF.REF_NUM5 IN ( 3 )
        THEN 1
        ELSE 0
      END AS ISBACKORDER,
      CASE
        WHEN PL.Purchase_Orders_Line_Status = 940
        THEN CR.Reason_Code_ID
      END AS CancelReasonCode,
      CASE
        WHEN PL.PURCHASE_ORDERS_LINE_STATUS = 940
        THEN CR.REASON_CODE
      END AS CANCELREASONDESC,
      CASE
        WHEN PL.Purchase_Orders_Line_Status = 940
        THEN PL.Last_Updated_DTTM
      END                 AS CancelDate,
      RR.Return_reason_ID AS ReturnReasonCode,
      SC.Code_Desc        AS ReturnReasonDesc,
      CASE
        WHEN P.Order_category = 3
        THEN pL.Created_Source
      END              AS CSRID,
      P.Customer_Email AS UsedEmail,
      P.Customer_Phone AS UsedPhoneNbr,
      BC.BILL_TO_FIRST_NAME,
      BC.BILL_TO_LAST_NAME,
      BC.BILL_TO_ADDRESS_LINE1,
      BC.BILL_TO_ADDRESS_LINE2,
      BC.BILL_TO_CITY,
      BC.BILLTO_STATE_PROV,
      BC.BILL_TO_POSTAL_CODE,
      BC.BILL_TO_COUNTRY_CODE,
      PL.D_Name,
      PL.D_Last_Name,
      PL.D_Address_1,
      PL.D_Address_2,
      PL.D_City,
      PL.D_State_Prov,
      PL.D_Postal_Code,
      PL.D_Country_Code,
      CASE WHEN PL.ORDER_FULFILLMENT_OPTION IN('01') THEN 1 ELSE 0 END AS ISBOPIS
    FROM ca.PURCHASE_ORDERS P
    JOIN po_cte
    ON po_cte.purchase_orders_id=p.purchase_orders_id
    JOIN ca.PO_REF_FIELDS PF
    ON p.PURCHASE_ORDERS_ID = PF.PURCHASE_ORDERS_ID
    JOIN ca.PURCHASE_ORDERS_LINE_ITEM PL
    ON P.Purchase_Orders_id        = PL.Purchase_Orders_ID
    AND PL.Parent_PO_Line_Item_ID IS NULL
    AND pl.last_updated_dttm      >=p.created_dttm
    JOIN ca.ITEM_CBO C
    ON PL.SKU = C.Item_Name
    LEFT OUTER JOIN ca.PO_LINE_ITEM_REF_FIELDS plf
    ON pl.PURCHASE_ORDERS_ID            = PLF.pURCHASE_ORDERS_id
    AND PL.purchase_orders_line_item_id = PLF.PURCHASE_ORDERS_LINE_ITEM_ID
    LEFT OUTER JOIN ca.a_discount_detail DD
    ON PL.purchase_orders_id            = DD.entity_id
    AND PL.purchase_orders_line_item_id = DD.entity_line_id
    AND dd.mark_for_deletion            = 0
    JOIN ca.ORDER_TYPE OT
    ON P.Order_category = OT.Order_Type_ID
    LEFT OUTER JOIN TAX_CTE T
    ON PL.purchase_orders_id            = T.entity_id
    AND PL.purchase_orders_line_item_id = T.entity_line_id --and PL.Order_QTY <> 0
    LEFT OUTER JOIN ca.A_CO_REASON_CODE CR
    ON P.Reason_ID = CR.Reason_Code_ID
    LEFT OUTER JOIN ca.RLM_RETURN_ORDERS_LINE_ITEM RR
    ON PL.Purchase_Orders_ID            = RR.Return_Orders_ID
    AND PL.purchase_orders_line_item_id = RR.Return_Orders_Line_Item_ID
    LEFT OUTER JOIN DOM_MDA.SYS_CODE SC
    ON RR.Return_Reason_ID = SC.Code_ID
    AND SC.code_Type       = '032'
    LEFT OUTER JOIN ca.PURCHASE_ORDERS PR
    ON P.Parent_Purchase_Orders_ID = PR.Purchase_Orders_ID
    LEFT OUTER JOIN ca.PO_REF_FIELDS PFR
    ON p.Parent_Purchase_Orders_ID = pfr.purchase_orders_id
    LEFT OUTER JOIN ca.ORDER_TYPE ODT
    ON PR.ORDER_CATEGORY = ODT.ORDER_TYPE_ID
    LEFT OUTER JOIN ca.PURCHASE_ORDERS_LINE_ITEM PLR
    ON PR.Purchase_Orders_id        = PLR.Purchase_Orders_ID
    AND PLR.Parent_PO_Line_Item_ID IS NULL
    AND PL.ORIGINAL_PO_LINE_ITEM_ID =PL.PURCHASE_ORDERS_LINE_ITEM_ID
    LEFT JOIN BILL_CTE BC
    ON P.PURCHASE_ORDERS_ID = BC.PURCHASE_ORDERS_ID
    WHERE p.last_updated_dttm > v_I_PROCESS_START_DATE AND P.Purchase_Orders_Date_DTTM <= v_I_PROCESS_END_DATE
    AND p.PURCHASE_ORDERS_ID      = PF.PURCHASE_ORDERS_ID
    AND ( p.ORDER_CATEGORY NOT   IN (3, 83)
    OR (p.ORDER_CATEGORY         IN (3)
    AND p.PURCHASE_ORDERS_STATUS != 20))
    GROUP BY P.Purchase_Orders_ID,
      P.TC_Purchase_Orders_Id,
      P.Purchase_Orders_Date_DTTM,
      PL.PURCHASE_ORDERS_LINE_ITEM_ID,
      PL.TC_PO_LINE_ID,
      P.Customer_Code,
      pf.ref_field2,
      PL.SKU,
      PL.ORIG_ORDER_QTY,
      PL.ORDER_QTY,
      PL.Unit_Monetary_Value,
      PL.TOTAL_MONETARY_VALUE,
      DECODE( pl.ORDER_FULFILLMENT_OPTION, '03', 'Ship to address', '02', 'Ship to store','01','BOPIS' ),
      PL.purchase_orders_line_item_ID,
      PLF.Ref_nUM3,
      pl.Purchase_Orders_Line_Status,
      PL.Is_Gift,
      C.Ref_Field1,
      C.Ref_Field2,
      plf.Ref_Num5,
      NVL(pf.ref_field1,PFR.ref_field1),
      CR.Reason_Code_ID,
      CR.Reason_Code,
      RR.Return_reason_ID,
      SC.Code_Desc,
      OT.ORDER_TYPE,
      P.Order_category,
      pl.Created_Source,
      P.Entry_Code,
      PR.TC_Purchase_Orders_ID,
      PR.Purchase_Orders_Date_DTTM,
      PL.Last_Updated_DTTM,
      P.Customer_Email,
      P.Customer_Phone,
      BC.BILL_TO_FIRST_NAME,
      BC.BILL_TO_LAST_NAME,
      BC.BILL_TO_ADDRESS_LINE1,
      BC.BILL_TO_ADDRESS_LINE2,
      BC.BILL_TO_CITY,
      BC.BILLTO_STATE_PROV,
      BC.BILL_TO_POSTAL_CODE,
      BC.BILL_TO_COUNTRY_CODE,
      PL.D_Name,
      PL.D_Last_Name,
      PL.D_Address_1,
      PL.D_Address_2,
      PL.D_City,
      PL.D_State_Prov,
      PL.D_Postal_Code,
      PL.D_Country_Code,
      ODT.ORDER_TYPE,
      DECODE(plr.ORDER_FULFILLMENT_OPTION,'03','Ship to address','02','Ship to store','01','BOPIS'),PL.ORDER_FULFILLMENT_OPTION
    ) a
  LEFT JOIN SHP_CTE SC
  ON a.PURCHASE_ORDERS_ID     = SC.PURCHASE_ORDERS_ID
  AND a.POLIID                = SC.CLIENTLNENUM
  --AND SC.O_FACILITY_ALIAS_ID IN (select O_FACILITY_ALIAS_ID from HTTD_FACILITY_ID_CONFIG where brand='TD')
  LEFT JOIN CANCEL_CTE CC
  ON a.PURCHASE_ORDERS_ID = CC.PURCHASE_ORDERS_ID
  AND a.POLIID            = CC.CLIENTLNENUM
  LEFT JOIN STS_CTE STS
  ON a.PURCHASE_ORDERS_ID = STS.PURCHASE_ORDERS_ID
  AND a.ITEMNUMBER        = STS.ITEM_NAME
  AND STS.MO_LINE_ITEM_ID =a.POLIID
    );

  v_step:= 'Update titlecode if no titlecode exists for parent or actual order in td_CRM_ORDDTL';
  UPDATE td_CRM_ORDDTL l
  SET TITLECODE=
    (SELECT NVL(TITLECODE, ORIGINALTITLECODE)
    FROM HT_INFC.TD_CRM_ORDHEAD k
    WHERE k.PURCHASE_ORDERS_ID= l.PURCHASE_ORDERS_ID
    )
  WHERE TITLECODE IS NULL;

  update TD_CRM_ORDDTL cod set linetaxamount=
  (select nvl(abs(sum(td.tax_amount)),0) from
  ca.a_invoice i,
  ca.A_INVOICE_LINE il,
  CA.PURCHASE_ORDERS po,
  ca.A_TAX_DETAIL td
  where po.tc_purchase_orders_id =cod.ORIGORDERNUMBER
  and po.purchase_orders_id=i.entity_id
  and i.invoice_id=il.invoice_id
  and i.invoice_id = td.entity_id
  and il.invoice_line_id=td.entity_line_id
  and cod.clientlnenum=il.return_order_line_id
  and td.tax_amount<>0)
  ,
  dollarvaluenet=
  (select dollarvaluenet-nvl(abs(sum(td.tax_amount)),0) from
  ca.a_invoice i,
  ca.A_INVOICE_LINE il,
  CA.PURCHASE_ORDERS po,
  ca.A_TAX_DETAIL td
  where po.tc_purchase_orders_id =cod.ORIGORDERNUMBER
  and po.purchase_orders_id=i.entity_id
  and i.invoice_id=il.invoice_id
  and i.invoice_id = td.entity_id
  and il.invoice_line_id=td.entity_line_id
  and cod.clientlnenum=il.return_order_line_id
  and td.tax_amount<>0)
  ,
  dollarvaluegross=
  (select dollarvaluegross-nvl(abs(sum(td.tax_amount)),0) from
  ca.a_invoice i,
  ca.A_INVOICE_LINE il,
  CA.PURCHASE_ORDERS po,
  ca.A_TAX_DETAIL td
  where po.tc_purchase_orders_id =cod.ORIGORDERNUMBER
  and po.purchase_orders_id=i.entity_id
  and i.invoice_id=il.invoice_id
  and i.invoice_id = td.entity_id
  and il.invoice_line_id=td.entity_line_id
  and cod.clientlnenum=il.return_order_line_id
  and td.tax_amount<>0)
  where ordermethod='Return'
  and linetaxamount =0 ;



  v_step          := 'Delete from td_crm_ordpay';
  --DELETE FROM td_crm_ordpay;
  execute immediate 'truncate table td_crm_ordpay';
  v_step:= 'Insert into td_crm_ordpay';
  INSERT INTO td_crm_ordpay
  SELECT
    /*+ LEADING (P)*/
    ORDERDATE,
    OrderDate AS invoice_date, --TO_Date(to_char(A.Invoice_creation_DTTM,'mm/dd/yyyy HH24:MI:SS'),'mm/dd/yyyy HH24:MI:SS') AS Invoice_date,
    OrderNumber,
    NVL(p.TitleCode, P.Originaltitlecode),
    -- A.Invoice_ID AS Sequence_ID,
    pt.payment_transaction_id AS Sequence_ID,
    PD.Card_type_ID,
    PM.Description,
    CASE
      WHEN PD.Card_type_ID IS NULL
      THEN ''
      ELSE PD.Card_number
    END AS CardNumber,
    OrderMethod,
    SUBSTR (pd.card_display_number, -4, 4) AS cc_no,
    CC.CARD_NAME,
    --sum(IP.PT_INVOICE_AMOUNT) as PROCESSED_AMOUNT,
    SUM(PT.PROCESSED_AMOUNT) AS PROCESSED_AMOUNT,
    SYSDATE                  AS EXTRACTDATE
  FROM TD_CRM_ORDHEAD p
  JOIN ca.A_Payment_Detail PD
  ON P.Purchase_Orders_ID = PD.Entity_ID
  JOIN CA.A_PAYMENT_TRANSACTION PT
  ON PT.PAYMENT_DETAIL_ID  =PD.PAYMENT_DETAIL_ID
  AND PT.PAYMENT_TRANS_TYPE=1
  LEFT JOIN ca.Payment_method PM
  ON PD.Payment_method = PM.Payment_Method
  LEFT JOIN ca.A_CREDIT_CARD CC
  ON PD.CARD_TYPE_ID = CC.CARD_ID
  GROUP BY ORDERDATE,
    ORDERNUMBER,
    NVL(p.TitleCode, P.Originaltitlecode),
    pt.payment_transaction_id,
    PD.Card_type_ID,
    PM.Description,
    CASE
      WHEN PD.Card_type_ID IS NULL
      THEN ''
      ELSE PD.Card_number
    END,
    OrderMethod,
    SUBSTR (PD.CARD_DISPLAY_NUMBER, -4, 4),
    CC.Card_Name,
    P.Originaltitlecode ;
  --select /*+ LEADING (P)*/
  --       OrderDate,
  --       TO_Date(to_char(A.Invoice_creation_DTTM,'mm/dd/yyyy HH24:MI:SS'),'mm/dd/yyyy HH24:MI:SS') AS Invoice_date,
  --       OrderNumber,
  --       TitleCode,
  --      -- A.Invoice_ID AS Sequence_ID,
  --       pt.payment_transaction_id AS Sequence_ID,
  --       PD.Card_type_ID,
  --       PM.Description,
  --       CASE WHEN PD.Card_type_ID IS NULL THEN '' ELSE PD.Card_number END AS CardNumber,
  --       OrderMethod,
  --       SUBSTR (pd.card_display_number, -4, 4) AS cc_no,
  --       CC.Card_Name,
  --       SUM(IP.pt_invoice_amount) as PROCESSED_AMOUNT,
  --     --  PT.PROCESSED_AMOUNT,
  --       sysdate as EXTRACTDATE
  --  FROM ht_infc.Td_CRM_ORDHEAD P
  --       JOIN ca.A_INVOICE A ON P.Purchase_Orders_ID = A.Entity_ID
  --       JOIN ca.A_Payment_Detail PD ON P.Purchase_Orders_ID = PD.Entity_ID
  --       join ca.a_payment_transaction pt on PT.PAYMENT_DETAIL_ID=PD.PAYMENT_DETAIL_ID and PT.PAYMENT_TRANS_TYPE=2
  --        join ca.A_PT_INVOICE_MAPPING IP ON  (IP.payment_transaction_id  =pt.payment_transaction_id and IP.invoice_id=A.invoice_id)
  --       LEFT JOIN ca.Payment_method PM
  --          ON PD.Payment_method = PM.Payment_Method
  --       left join ca.A_CREDIT_CARD CC on PD.CARD_TYPE_ID = CC.CARD_ID
  -- WHERE     (   A.last_updated_dttm BETWEEN v_I_PROCESS_START_DATE and v_I_PROCESS_END_DATE)
  --       AND
  --       A.Is_Published = 1
  --       --and OrderNumber = '147512'
  -- Group By OrderDate, TO_Date(to_char(A.Invoice_creation_DTTM,'mm/dd/yyyy HH24:MI:SS'),'mm/dd/yyyy HH24:MI:SS'), OrderNumber, TitleCode,
  --  pt.payment_transaction_id,
  --       PD.Card_type_ID,
  --       PM.Description,
  --       CASE WHEN PD.Card_type_ID IS NULL THEN '' ELSE PD.Card_number END ,
  --       OrderMethod,
  --       SUBSTR (pd.card_display_number, -4, 4),
  --       CC.Card_Name;
  COMMIT;

  v_step:= 'Delete from td_CRM_ORDPROMO';
  --DELETE FROM td_CRM_ORDPROMO;
  execute immediate 'truncate table td_CRM_ORDPROMO';

  v_step:= 'Insert into td_CRM_ORDPROMO';
  INSERT INTO td_CRM_ORDPROMO
  SELECT
    /*+ LEADING (P)*/
    PL.SKU AS ItemNumber,
    OrderNumber,
    OrderDate,
    TitleCode AS StoreNumber,
    NVL(p.TitleCode, P.Originaltitlecode),
    NVL (PL.Root_Line_Item_ID, PL.Purchase_Orders_Line_Item_ID) AS ClientLneNum,
    1                                                           AS PromoLineSeqNumber,
    External_Discount_ID                                        AS PromoNumber,
    PL.ORIG_ORDER_QTY                                           AS Quantity,
    DD.Discount_Amount                                          AS UnitDiscountAmt,
    sysdate                                                     AS EXTRACTDATE,
    DD.DISCOUNT_DETAIL_ID                                       AS DiscountSequence
  FROM td_crm_ordhead P
  JOIN ca.PURCHASE_ORDERS_LINE_ITEM PL
  ON P.Purchase_Orders_id              = PL.Purchase_Orders_ID
  AND PL.Parent_PO_Line_Item_ID       IS NULL
  AND pl.created_dttm                  > = p.created_datetime
  AND (PL.Order_QTY                   <> 0
  AND PL.Order_qty - PL.Cancelled_Qty <> 0)
  LEFT OUTER JOIN ca.PO_LINE_ITEM_REF_FIELDS plf
  ON pl.PURCHASE_ORDERS_ID            = PLF.pURCHASE_ORDERS_id
  AND PL.PURCHASE_ORDERS_LINE_ITEM_ID = PLF.PURCHASE_ORDERS_LINE_ITEM_ID
  LEFT JOIN ca.A_DISCOUNT_DETAIL DD
  ON PL.purchase_orders_id                                        = DD.entity_id
  AND NVL (PL.Root_Line_Item_ID, PL.purchase_orders_line_item_id) = DD.entity_line_id
  AND DD.MARK_FOR_DELETION                                        = 0
  WHERE -- p.CREATED_DATETIME between I_PROCESS_START_DATE and I_PROCESS_END_DATE             and
    External_Discount_ID IS NOT NULL;
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE(SQLERRM||' in execution of step'||v_step);
  RAISE;
END td_CRM_ORDINFO;
/
